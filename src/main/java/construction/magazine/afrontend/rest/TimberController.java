package construction.magazine.afrontend.rest;

import construction.magazine.business.DTO.TimberDTO;
import construction.magazine.business.service.TimberService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
//mapam adresa dupa care trebuie sa cautam in browser(localhost:8080/timber/...putem face mai multe metode)
@RequestMapping("/timber/")
public class TimberController {

    @Autowired
    TimberService timberService;

    @GetMapping(path="getListOfTimber")
    public List<TimberDTO> getListOfTimber(){
        List<TimberDTO> timberDTOS = new LinkedList<>();
        return timberDTOS = timberService.findListOfTimberDTO();
    }

    @GetMapping(path = "getListOfTimberByParameters", consumes = "application/json")
    public List<TimberDTO> getListOfTimberByParameters(@RequestBody TimberDTO timberDTO){
        return timberService.findTimberByParameters(timberDTO);
    }

    @PostMapping(path = "addTimber", consumes = "application/json")
    public ResponseEntity addBeam(@RequestBody TimberDTO timberDTO){
        timberService.addBeam(timberDTO);
        return ResponseEntity.ok().build();
    }
}
