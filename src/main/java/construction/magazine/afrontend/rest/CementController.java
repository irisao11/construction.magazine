package construction.magazine.afrontend.rest;

import construction.magazine.business.DTO.CementDTO;
import construction.magazine.business.service.CementService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/cement/")
public class CementController {

    @Autowired
    CementService cementService;

    @GetMapping("getCementList")
    public List<CementDTO> getCementList(){
        List<CementDTO> cementDTOS = new LinkedList<CementDTO>();
        return cementDTOS = cementService.findListOfCement();
    }

    @GetMapping("getCementBySeller/{seller}")
    public List<CementDTO> getCementBySeller(@PathVariable String seller){
        List<CementDTO> cementDTOS = cementService.findListbySeller(seller);
        return cementDTOS;
    }

    @PostMapping(path = "addCement", consumes = "application/json")
    public ResponseEntity addCement(@RequestBody CementDTO cementDTO){
        cementService.addCement(cementDTO);
        return ResponseEntity.ok().build();
    }
}
