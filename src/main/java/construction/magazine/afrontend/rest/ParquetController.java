package construction.magazine.afrontend.rest;

import construction.magazine.business.DTO.BrickDTO;
import construction.magazine.business.DTO.ParquetDTO;
import construction.magazine.business.service.ParquetService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/parquet/")
public class ParquetController {

    @Autowired
    ParquetService parquetService;

    @GetMapping(path = "getListOfParquet/{type}")
    public List<ParquetDTO> getListOfParquet(@PathVariable String type) {
        List<ParquetDTO> parquetDTOS = new LinkedList<>();
        return parquetDTOS = parquetService.findParquetByType(type);
    }

    @PostMapping(path = "addParquet", consumes = "application/json")
    public ResponseEntity addParquet(@RequestBody ParquetDTO parquetDTO){
        parquetService.addParquet(parquetDTO);
        return ResponseEntity.ok().build();
    }
}
