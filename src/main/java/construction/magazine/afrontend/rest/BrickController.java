package construction.magazine.afrontend.rest;

import construction.magazine.business.DTO.BrickDTO;
import construction.magazine.business.service.BrickService;
import construction.magazine.persistance.entities.Brick;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.LinkedList;
import java.util.List;

@RestController
@RequestMapping("/bricks/")
public class BrickController {

    @Autowired
    BrickService brickService;

    @PostMapping(path = "addBrick", consumes = "application/json")
    public ResponseEntity addBrick(@RequestBody BrickDTO brickDTO){
        brickService.addBrick(brickDTO);
        return ResponseEntity.ok().build();
    }

    @GetMapping(path = "getListOfBricks")
    public List<BrickDTO> getListOfBricks() {
        List<BrickDTO> brickDTOS = new LinkedList<>();
        return brickDTOS = brickService.findListOfDTO();
    }

    @GetMapping(path = "getBrickOfType/{type}")
    public List<BrickDTO> getBrickOfType(@PathVariable String type) {
        List<BrickDTO> brickDTOS = new LinkedList<>();
        return brickDTOS = brickService.findBrickType(type);
    }

    @GetMapping(path = "getBricksByParameters", consumes = "application/json")
    public List<BrickDTO> getBricksByParameters(@RequestBody BrickDTO brickDTO){
        return brickService.findBrickDTOSByParameters(brickDTO);
    }

    @DeleteMapping(path = "deleteBrickByQuantity/{quantity}")
    public ResponseEntity deleteBrickByQuantity(@PathVariable int quantity) {
        if (quantity == 0) {
            int rowsDeleted = brickService.deleteBrickByQuantity(quantity);
            return ResponseEntity.ok("the num of rows deleted is " + rowsDeleted);
        } else {
            return ResponseEntity.badRequest().body("nu a fost sters");
        }
    }

    @DeleteMapping(path = "deleteBrickOfType/{type}")
    public ResponseEntity deleteBrickOfType(@PathVariable String type) {

        int rowsDeleted = brickService.deleteBrickbyType(type);
        return ResponseEntity.ok(rowsDeleted + " bricks of type " + type + " deleted");
    }
}
