package construction.magazine.afrontend.rest;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/")
public class IndexController {
    @GetMapping(path = "welcome")
    public String welcome() {
        String welcome = "Bine ati venit pe pagina de inceput!";
        return welcome;
    }

    @GetMapping(path = "goodBye")
    public String goodBye(){
        String goodBye = "La revedere!";
        return goodBye;
    }


}
