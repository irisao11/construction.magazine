package construction.magazine.afrontend;

import construction.magazine.business.DTO.BrickDTO;
import construction.magazine.business.service.BrickService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import construction.magazine.persistance.config.HibernateUtil;
import org.springframework.context.annotation.ComponentScan;

import java.util.List;
import java.util.Scanner;

@SpringBootApplication
@ComponentScan("construction.magazine.*")
public class Main {

    public static void main(String[] args) {
        SpringApplication.run(Main.class, args);
        //BrickService brickService;
        //HibernateUtil hibernateUtil = new HibernateUtil();


        //AnnotationConfigApplicationContext applicationContext = new AnnotationConfigApplicationContext(DIConfig.class);
        //brickService = applicationContext.getBean(BrickService.class);

        //Scanner s = new Scanner(System.in);
        //System.out.println("Insert brick type");
       // String type = s.next();
       // List<BrickDTO> brickDTOS = brickService.findBrickType(type);
        //for(BrickDTO brickDTO:brickDTOS){
           // System.out.println(brickDTO);
        //}
    }
}
