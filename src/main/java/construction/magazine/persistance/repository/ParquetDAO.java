package construction.magazine.persistance.repository;

import construction.magazine.persistance.config.HibernateUtil;
import construction.magazine.persistance.entities.Parquet;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.List;

@Repository
public class ParquetDAO {
    HibernateUtil hibernateUtil;

    public ParquetDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void insertParquet(Parquet parquet) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        session.persist(parquet);
        transaction.commit();
        session.close();
    }

    public List<Parquet> findParquetByParameters(Parquet parquet){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query findParquetByParameters = session.createNamedQuery("find_parquet_by_parameters");
        findParquetByParameters.setParameter("type", parquet.getType())
                               .setParameter("colour", parquet.getColour());
        List<Parquet> listOfParquetByParameters = findParquetByParameters.getResultList();
        transaction.commit();
        session.close();
        return listOfParquetByParameters;
    }

    public List<Parquet> findParquetByType(String type) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query findParquetByType = session.createNamedQuery("find_parquet_by_type");
        findParquetByType.setParameter("type", type);
        List<Parquet> parquetList = findParquetByType.getResultList();
        transaction.commit();
        session.close();
        return parquetList;
    }
    
    public void deleteParquet(String type){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query deleteParquetByType = session.createNamedQuery("delete_parquet_by_type");
        deleteParquetByType.setParameter("type", type);
        deleteParquetByType.executeUpdate();
        transaction.commit();
        session.close();
    }
}
