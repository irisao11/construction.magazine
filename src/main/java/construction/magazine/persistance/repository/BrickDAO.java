package construction.magazine.persistance.repository;

import construction.magazine.persistance.entities.Timber;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import construction.magazine.persistance.config.HibernateUtil;
import construction.magazine.persistance.entities.Brick;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class BrickDAO {

    private HibernateUtil hibernateUtil;

    public BrickDAO() {
        hibernateUtil = new HibernateUtil();
    }

    public void insertBrick(Brick brick) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        session.persist(brick);
        transaction.commit();
        session.close();
    }

    public int deleteBrick(String type) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query deleteQuery = session.createNamedQuery("delete_brick_by_type");
        deleteQuery.setParameter("type", type);
        int rowsDeleted = deleteQuery.executeUpdate();
        transaction.commit();
        session.close();
        return rowsDeleted;
    }

    public int deleteBrickByQuantity(int quantity) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query deleteBrickByQuantity = session.createNamedQuery("delete_brick_by_quantity");
        deleteBrickByQuantity.setParameter("quantity", quantity);
        int numOfRowsDeleted = deleteBrickByQuantity.executeUpdate();
        transaction.commit();
        session.close();
        return numOfRowsDeleted;
    }

    public List<Brick> selectBrick(String type) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query selectBrick = session.createNamedQuery("find_brick_by_type");
        selectBrick.setParameter("type", type);
        List<Brick> students = selectBrick.getResultList();
        transaction.commit();
        session.close();
        return students;
    }

    public List<Brick> findListOfBricks() {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        List<Brick> brickList = new ArrayList<Brick>();
        Query selectBrickList = session.createNamedQuery("find_brick");
        brickList = selectBrickList.getResultList();
        transaction.commit();
        session.close();
        return brickList;
    }

    public List<Brick> findListOfBricksByParameters(Brick brick) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        List<Brick> brickListByParameters = new ArrayList<Brick>();
        Query selectBrickByParameters = session.createNamedQuery("find_bricks_by_parameters");
        selectBrickByParameters.setParameter("type", brick.getType())
                               .setParameter("seller", brick.getSeller());
        brickListByParameters = selectBrickByParameters.getResultList();

        transaction.commit();
        session.close();

        return brickListByParameters;
    }

    public void updateBrick(Brick brick) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query updateBrick = session.createNamedQuery("update_brick");
        updateBrick.setParameter("type", brick.getType()).setParameter("seller", brick.getSeller()).setParameter("price", brick.getPrice()).setParameter("quantity", brick.getQuantity());
        updateBrick.executeUpdate();
        transaction.commit();
        session.close();
    }
}
