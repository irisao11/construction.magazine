package construction.magazine.persistance.repository;

import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;
import construction.magazine.persistance.config.HibernateUtil;
import construction.magazine.persistance.entities.Cement;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class CementDAO {

    private HibernateUtil hibernateUtil;

    public CementDAO(){
        hibernateUtil = new HibernateUtil();
    }

    public void insertCement(Cement cement) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        session.persist(cement);
        transaction.commit();
        session.close();
    }

    public void deleteCement(String type) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query deleteCement = session.createNamedQuery("delete_cement_by_type");
        deleteCement.setParameter("type", type);
        deleteCement.executeUpdate();
        transaction.commit();
        session.close();
    }

    public List<Cement> selectCement(String type){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query selectCement = session.createNamedQuery("find_cement_by_type");
        selectCement.setParameter("type", type);
        List<Cement> cementList = selectCement.getResultList();
        transaction.commit();
        session.close();
        return cementList;
    }

    public List<Cement> findCementBySeller(String seller){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query findCementBySeller = session.createNamedQuery("find_cement_by_seller");
        findCementBySeller.setParameter("seller", seller);
        List<Cement> cementList = findCementBySeller.getResultList();
        transaction.commit();
        session.close();
        return cementList;
    }

    public List<Cement> findCementList(){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        List<Cement> cementList = new ArrayList<Cement>();
        Query findCementList = session.createNamedQuery("find_cement_list");
        cementList = findCementList.getResultList();
        transaction.commit();
        session.close();
        return cementList;
    }

    public void updateCement(Cement cement){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        Query updateBrick = session.createNamedQuery("update_brick");
        updateBrick.setParameter("type", cement.getType()).setParameter("seller", cement.getSeller()).setParameter("price", cement.getPrice()).setParameter("quantity", cement.getQuantity());
        updateBrick.executeUpdate();
        transaction.commit();
        session.close();
    }
}
