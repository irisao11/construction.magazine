package construction.magazine.persistance.repository;

import construction.magazine.persistance.config.HibernateUtil;
import construction.magazine.persistance.entities.Brick;
import construction.magazine.persistance.entities.Timber;
import org.hibernate.Hibernate;
import org.hibernate.Session;
import org.hibernate.Transaction;
import org.springframework.stereotype.Repository;

import javax.persistence.Query;
import java.util.ArrayList;
import java.util.List;

@Repository
public class TimberDAO {

    HibernateUtil hibernateUtil;

    public TimberDAO() {
        hibernateUtil =new HibernateUtil();
    }

    public List<Timber> findListOfTimberByParameters(Timber timber){
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();

        List<Timber> timberList = new ArrayList<Timber>();
        Query selectTimberByParameters = session.createNamedQuery("find_timber_by_parameters");
        selectTimberByParameters.setParameter("woodType", timber.getWoodType()).setParameter("quantity", timber.getQuantity());
        timberList = selectTimberByParameters.getResultList();

        transaction.commit();
        session.close();
        return timberList;
    }

    public List<Timber> findListOfTimber() {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        List<Timber> timberList = new ArrayList<Timber>();
        Query selectTimberList = session.createNamedQuery("find_timber");
        timberList = selectTimberList.getResultList();
        transaction.commit();
        session.close();
        return timberList;
    }

    public void addTimber(Timber timber) {
        Session session = hibernateUtil.getSession();
        Transaction transaction = session.beginTransaction();
        session.persist(timber);
        transaction.commit();
        session.close();
    }
}
