package construction.magazine.persistance.entities;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;

@Entity
@Table(name="ferestre")
public class Window {

    @Id
    private int id;

    @Column(name = "tip")
    private String type;

    @Column(name="lungime")
    private int length;

    @Column(name="latime")
    private int width;

    @Column(name="pret")
    private int price;

    @Column(name="cantitate")
    private int quantity;

    public Window() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Window window = (Window) o;
        return id == window.id &&
                length == window.length &&
                width == window.width &&
                price == window.price &&
                quantity == window.quantity &&
                Objects.equals(type, window.type);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, length, width, price, quantity);
    }

    @Override
    public String toString() {
        return "Window{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", length=" + length +
                ", width=" + width +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
