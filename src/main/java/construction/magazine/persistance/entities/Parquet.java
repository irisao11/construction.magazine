package construction.magazine.persistance.entities;

import javax.persistence.*;
import java.util.Objects;
@NamedQueries({
        @NamedQuery(name = "find_parquet_by_type",
                query = "select p from Parquet p where p.type = :type"),
        @NamedQuery(name = "delete_parquet_by_type",
                query = "delete from Parquet p where p.type = :type"),
        @NamedQuery(name = "find_parquet_list",
                query = "select p from Parquet p"),
        @NamedQuery(name = "update_parquet",
                query = "update Parquet p set p.type = :type, p.length = :length, p.width = :width, p.colour = :colour, p.price = :price, p.quantity = :quantity"),
        @NamedQuery(name = "find_parquet_by_parameters",
                query = "select p from Parquet p where p.type = :type and p.colour = :colour")

})

@Entity
@Table(name = "parchet")
public class Parquet {

    @Id
    private int id;

    @Column(name = "tip")
    private String type;

    @Column(name="lungime")
    private int length;

    @Column(name="latime")
    private int width;

    @Column(name="culoare")
    private String colour;

    @Column(name="pret")
    private int price;

    @Column(name="cantitate")
    private int quantity;

    public Parquet() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Parquet parquet = (Parquet) o;
        return id == parquet.id &&
                length == parquet.length &&
                width == parquet.width &&
                price == parquet.price &&
                quantity == parquet.quantity &&
                Objects.equals(type, parquet.type) &&
                Objects.equals(colour, parquet.colour);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, length, width, colour, price, quantity);
    }

    @Override
    public String toString() {
        return "Parquet{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", length=" + length +
                ", width=" + width +
                ", colour='" + colour + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
