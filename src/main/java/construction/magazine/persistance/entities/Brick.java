package construction.magazine.persistance.entities;

import javax.persistence.*;
import java.util.Objects;
@NamedQueries({
        @NamedQuery(name = "delete_brick_by_type",
                query = "delete from Brick b where b.type = :type"),
        @NamedQuery(name = "find_brick_by_type",
                query = "select b from Brick b where b.type = :type"),
        @NamedQuery(name = "update_brick",
                query = "update Brick b set b.type = :type, b.seller = :seller, b.price = :price, b.quantity = :quantity"),
        @NamedQuery(name = "find_brick",
                query = "select b from Brick b"),
        @NamedQuery(name="delete_brick_by_quantity",
                query = "delete from Brick b where b.quantity= :quantity"),
        @NamedQuery(name = "find_bricks_by_parameters",
                query = "select b from Brick b where b.type = :type and b.seller = :seller")

})

@Entity
@Table(name="caramizi")
public class Brick {

    @Id
    private int id;

    @Column(name = "tip")
    private String type;

    @Column(name = "producator")
    private String seller;

    @Column(name = "pret")
    private int price;

    @Column(name = "cantitate")
    private int quantity;

    public Brick() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Brick brick = (Brick) o;
        return id == brick.id &&
                price == brick.price &&
                quantity == brick.quantity &&
                Objects.equals(type, brick.type) &&
                Objects.equals(seller, brick.seller);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, seller, price, quantity);
    }

    @Override
    public String toString() {
        return "Brick{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", seller='" + seller + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
