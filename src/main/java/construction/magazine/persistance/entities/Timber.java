package construction.magazine.persistance.entities;

import javax.persistence.*;
import javax.validation.constraints.NotEmpty;
import javax.validation.constraints.NotNull;
import java.util.Objects;
@NamedQueries({
        @NamedQuery(name = "delete_timber_by_woodType",
                query = "delete from  Timber t where t.woodType = :woodType"),
        @NamedQuery(name = "find_timber_by_name",
                query = "select t from Timber t where t.woodType = :woodType"),
        //@NamedQuery(name = "update_timber",
               // query = "update Timber t set t.type = :type, t.seller = :seller, t.price = :price, t.quantity = :quantity"),
        @NamedQuery(name = "find_timber",
                query = "select t from Timber t"),
        @NamedQuery(name = "find_timber_by_parameters",
                query = "select t from Timber t where t.woodType = :woodType and t.quantity = :quantity")
})

@Entity
@Table(name="grinzi")
public class Timber {

    @Id
    private int id;

    @Column(name="lungime")
    private int length;

    @Column(name="grosime")
    private int height;

    @NotEmpty
    @Column(name="lemn")
    private String woodType;

    @NotNull
    @Column(name="pret")
    private int price;

    @Column(name="cantitate")
    private int quantity;

    public Timber() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getWoodType() {
        return woodType;
    }

    public void setWoodType(String woodType) {
        this.woodType = woodType;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Timber timber = (Timber) o;
        return id == timber.id &&
                length == timber.length &&
                height == timber.height &&
                price == timber.price &&
                quantity == timber.quantity &&
                Objects.equals(woodType, timber.woodType);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, length, height, woodType, price, quantity);
    }

    @Override
    public String toString() {
        return "Timber{" +
                "id=" + id +
                ", length=" + length +
                ", height=" + height +
                ", woodType='" + woodType + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
