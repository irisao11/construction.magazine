package construction.magazine.persistance.entities;

import javax.persistence.*;
import java.util.Objects;

@NamedQueries({
        @NamedQuery(name = "delete_cement_by_type",
                query = "delete from Cement c where c.type = :type"),
        @NamedQuery(name = "find_cement_by_type",
                query = "select c from Cement c where c.type = :type"),
        @NamedQuery(name = "update_cement",
                query = "update Cement c set c.type = :type, c.seller = :seller, c.price = :price, c.quantity = :quantity"),
        @NamedQuery(name = "find_cement_list",
                query = "select c from Cement c"),
        @NamedQuery(name = "find_cement_by_seller",
                query = "select c from Cement c where c.seller = :seller")

})

@Entity
@Table(name = "ciment")
public class Cement {

    @Id
    private int id;

    @Column(name = "tip")
    private String type;

    @Column(name = "producator")
    private String seller;

    @Column(name = "pret")
    private int price;

    @Column(name = "cantitate")
    private int quantity;

    public Cement() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Cement cement = (Cement) o;
        return id == cement.id &&
                price == cement.price &&
                quantity == cement.quantity &&
                Objects.equals(type, cement.type) &&
                Objects.equals(seller, cement.seller);
    }

    @Override
    public int hashCode() {
        return Objects.hash(id, type, seller, price, quantity);
    }

    @Override
    public String toString() {
        return "Cement{" +
                "id=" + id +
                ", type='" + type + '\'' +
                ", seller='" + seller + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
