package construction.magazine.business.DTO;

public class ParquetDTO {
    private String type;
    private int length;
    private int width;
    private String colour;
    private int price;
    private int quantity;

    public ParquetDTO() {
    }

    public ParquetDTO(String type, int length, int width, String colour, int price, int quantity) {
        this.type = type;
        this.length = length;
        this.width = width;
        this.colour = colour;
        this.price = price;
        this.quantity = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getWidth() {
        return width;
    }

    public void setWidth(int width) {
        this.width = width;
    }

    public String getColour() {
        return colour;
    }

    public void setColour(String colour) {
        this.colour = colour;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "ParquetDTO{" +
                "type='" + type + '\'' +
                ", length=" + length +
                ", width=" + width +
                ", colour='" + colour + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
