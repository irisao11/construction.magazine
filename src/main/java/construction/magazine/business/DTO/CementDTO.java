package construction.magazine.business.DTO;

public class CementDTO {
    private String type;
    private String seller;
    private int price;
    private int quantity;

    public CementDTO() {
    }

    public CementDTO(String type, String seller, int price, int quantity) {
        this.type = type;
        this.seller = seller;
        this.price = price;
        this.quantity = quantity;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getSeller() {
        return seller;
    }

    public void setSeller(String seller) {
        this.seller = seller;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "CementDTO{" +
                "type='" + type + '\'' +
                ", seller='" + seller + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
