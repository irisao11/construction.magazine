package construction.magazine.business.DTO;

public class TimberDTO {
    private int length;
    private int height;
    private String woodType;
    private int price;
    private int quantity;

    public TimberDTO() {
    }

    public TimberDTO(int length, int height, String woodType, int price, int quantity) {
        this.length = length;
        this.height = height;
        this.woodType = woodType;
        this.price = price;
        this.quantity = quantity;
    }

    public int getLength() {
        return length;
    }

    public void setLength(int length) {
        this.length = length;
    }

    public int getHeight() {
        return height;
    }

    public void setHeight(int height) {
        this.height = height;
    }

    public String getWoodType() {
        return woodType;
    }

    public void setWoodType(String woodType) {
        this.woodType = woodType;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    @Override
    public String toString() {
        return "TimberDTO{" +
                "length=" + length +
                ", height=" + height +
                ", woodType='" + woodType + '\'' +
                ", price=" + price +
                ", quantity=" + quantity +
                '}';
    }
}
