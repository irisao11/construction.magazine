package construction.magazine.business.service;

import construction.magazine.business.DTO.CementDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import construction.magazine.persistance.entities.Cement;
import construction.magazine.persistance.repository.CementDAO;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

@Service
public class CementService {

    @Autowired
    CementDAO cementDAO;

    public void addCement(CementDTO cementDTO) {
        Cement cement = new Cement();
        cement.setType(cementDTO.getType());
        cement.setSeller(cementDTO.getSeller());
        cement.setPrice(cementDTO.getPrice());
        cement.setQuantity(cementDTO.getQuantity());
        cementDAO.insertCement(cement);
    }

    public void deleteCement(String type){
        cementDAO.deleteCement(type);
    }

    public List<CementDTO> findCementByType(String type){
        List<Cement> cementList = cementDAO.selectCement(type);
        List<CementDTO> cementDTOS = new LinkedList<>();
        for(Cement c: cementList){
            CementDTO cementDTO = new CementDTO();
            cementDTO.setType(c.getType());
            cementDTO.setSeller(c.getSeller());
            cementDTO.setPrice(c.getPrice());
            cementDTO.setQuantity(c.getQuantity());
            cementDTOS.add(cementDTO);
        }
        return cementDTOS;
    }

    public List<CementDTO> findListbySeller(String seller){
        List<CementDTO> cementDTOS = new LinkedList<CementDTO>();
        List<Cement> cementList = cementDAO.findCementBySeller(seller);

        for(Cement c: cementList){
            CementDTO cementDTO = new CementDTO();
            cementDTO.setType(c.getType());
            cementDTO.setSeller(c.getSeller());
            cementDTO.setPrice(c.getPrice());
            cementDTO.setQuantity(c.getQuantity());
            cementDTOS.add(cementDTO);
        }
        return cementDTOS;
    }

    public List<CementDTO> findListOfCement(){
        List<CementDTO> cementDTOS = new LinkedList<CementDTO>();
        List<Cement> cementList = cementDAO.findCementList();

        for (int i = 0; i <cementList.size() ; i++) {
            CementDTO cementDTO = new CementDTO();

            cementDTO.setType(cementList.get(i).getType());
            cementDTO.setSeller(cementList.get(i).getSeller());
            cementDTO.setPrice(cementList.get(i).getPrice());
            cementDTO.setQuantity(cementList.get(i).getQuantity());
            cementDTOS.add(cementDTO);
        }
        return cementDTOS;
    }
    public void updateCement(String type){

    }
}
