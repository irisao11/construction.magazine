package construction.magazine.business.service;

import construction.magazine.business.DTO.BrickDTO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import construction.magazine.persistance.entities.Brick;
import construction.magazine.persistance.repository.BrickDAO;

import java.util.LinkedList;
import java.util.List;

@Service
public class BrickService {

    @Autowired
    BrickDAO brickDAO;


    public void addBrick(BrickDTO brickDTO) {
        Brick brick = new Brick();
        brick.setType(brickDTO.getType());
        brick.setSeller(brickDTO.getSeller());
        brick.setPrice(brickDTO.getPrice());
        brick.setQuantity(brickDTO.getQuantity());
        brickDAO.insertBrick(brick);
    }

    public int deleteBrickbyType(String type) {
        return brickDAO.deleteBrick(type);
    }

    public int deleteBrickByQuantity(int quantity) {
        return brickDAO.deleteBrickByQuantity(quantity);

    }

    public List<BrickDTO> findBrickType(String type) {
        List<Brick> bricks = brickDAO.selectBrick(type);
        List<BrickDTO> brickDTOS = new LinkedList<>();
        for (Brick b : bricks) {
            BrickDTO brickDTO = new BrickDTO();
            brickDTO.setType(b.getType());
            brickDTO.setSeller(b.getSeller());
            brickDTO.setPrice(b.getPrice());
            brickDTO.setQuantity(b.getQuantity());
            brickDTOS.add(brickDTO);
        }
        return brickDTOS;
    }

    public List<BrickDTO> findBrickDTOSByParameters(BrickDTO brickDTO) {

        Brick brick = new Brick();
        brick.setPrice(brickDTO.getPrice());
        brick.setQuantity(brickDTO.getQuantity());
        brick.setSeller(brickDTO.getSeller());
        brick.setType(brickDTO.getType());

        List<Brick> listOfBricksByParameters = brickDAO.findListOfBricksByParameters(brick);
        List<BrickDTO> listOfBrickDTOByParameters = new LinkedList<>();

        for (Brick b : listOfBricksByParameters) {
            BrickDTO bDTO = new BrickDTO();
            bDTO.setQuantity(b.getQuantity());
            bDTO.setPrice(b.getPrice());
            bDTO.setSeller(b.getSeller());
            bDTO.setType(b.getType());
            listOfBrickDTOByParameters.add(bDTO);
        }
        return listOfBrickDTOByParameters;
    }


    public List<BrickDTO> findListOfDTO() {
        List<BrickDTO> brickDTOS = new LinkedList<BrickDTO>();
        List<Brick> brickList = brickDAO.findListOfBricks();

        for (int i = 0; i < brickList.size(); i++) {
            BrickDTO brickDTO = new BrickDTO();

            brickDTO.setType(brickList.get(i).getType());
            brickDTO.setSeller(brickList.get(i).getSeller());
            brickDTO.setPrice(brickList.get(i).getPrice());
            brickDTO.setQuantity(brickList.get(i).getQuantity());

            brickDTOS.add(brickDTO);
        }

        return brickDTOS;
    }

    public void updateBricks(String type) {

    }
}
