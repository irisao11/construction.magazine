package construction.magazine.business.service;

import construction.magazine.business.DTO.ParquetDTO;
import construction.magazine.persistance.entities.Cement;
import construction.magazine.persistance.entities.Parquet;
import construction.magazine.persistance.repository.ParquetDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class ParquetService {

    @Autowired
    ParquetDAO parquetDAO;

    public void insertParquet(ParquetDTO parquetDTO){
        Parquet parquet = new Parquet();
        parquet.setType(parquetDTO.getType());
        parquet.setLength(parquetDTO.getLength());
        parquet.setWidth(parquetDTO.getWidth());
        parquet.setColour(parquetDTO.getColour());
        parquet.setPrice(parquetDTO.getPrice());
        parquet.setQuantity(parquetDTO.getQuantity());
        parquetDAO.insertParquet(parquet);
    }

    public List<ParquetDTO> findParquetByType(String type){
        List<ParquetDTO> parquetDTOS = new LinkedList<>();
        List<Parquet> parquetList = parquetDAO.findParquetByType(type);
        for(Parquet p: parquetList){
            ParquetDTO parquetDTO = new ParquetDTO();
            parquetDTO.setType(p.getType());
            parquetDTO.setLength(p.getLength());
            parquetDTO.setWidth(p.getWidth());
            parquetDTO.setColour(p.getColour());
            parquetDTO.setPrice(p.getPrice());
            parquetDTO.setQuantity(p.getQuantity());
            parquetDTOS.add(parquetDTO);
        }
    return parquetDTOS;
    }

    public void deleteParquet(String type){
        parquetDAO.deleteParquet(type);
    }

    public void addParquet(ParquetDTO parquetDTO){
        Parquet parquet = new Parquet();
        parquet.setQuantity(parquetDTO.getQuantity());
        parquet.setPrice(parquetDTO.getPrice());
        parquet.setColour(parquetDTO.getColour());
        parquet.setWidth(parquetDTO.getWidth());
        parquet.setLength(parquetDTO.getLength());
        parquet.setType(parquetDTO.getType());
        parquetDAO.insertParquet(parquet);
    }

    public List<ParquetDTO> findParquetByParameters(ParquetDTO parquetDTO){

    }
}
