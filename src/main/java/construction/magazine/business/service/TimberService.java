package construction.magazine.business.service;

import construction.magazine.business.DTO.BrickDTO;
import construction.magazine.business.DTO.TimberDTO;
import construction.magazine.persistance.entities.Brick;
import construction.magazine.persistance.entities.Timber;
import construction.magazine.persistance.repository.TimberDAO;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.LinkedList;
import java.util.List;

@Service
public class TimberService {

    @Autowired
    TimberDAO timberDAO;

    public List<TimberDTO> findListOfTimberDTO() {
        List<TimberDTO> timberDTOS = new LinkedList<TimberDTO>();
        List<Timber> timberList = timberDAO.findListOfTimber();

        for (int i = 0; i < timberList.size(); i++) {
            TimberDTO timberDTO = new TimberDTO();
            timberDTO.setLength(timberList.get(i).getLength());
            timberDTO.setHeight(timberList.get(i).getLength());
            timberDTO.setWoodType(timberList.get(i).getWoodType());
            timberDTO.setPrice(timberList.get(i).getPrice());
            timberDTO.setQuantity(timberList.get(i).getQuantity());
            timberDTOS.add(timberDTO);
        }

        return timberDTOS;
    }

    public List<TimberDTO> findTimberByParameters(TimberDTO timberDTO) {

        //incoming flow/request flow (vin datele din frontend)
        Timber timber = new Timber();
        timber.setQuantity(timberDTO.getQuantity());
        timber.setPrice(timberDTO.getPrice());
        timber.setLength(timberDTO.getLength());
        timber.setHeight(timberDTO.getHeight());
        timber.setWoodType(timberDTO.getWoodType());

        //callul catre BD
        //the place where the persistance layer meets business layer
        List<Timber> listOfTimberByParameters = timberDAO.findListOfTimberByParameters(timber);
        List<TimberDTO> listOfTimberDTOByParameters = new LinkedList<>();

        //outgoingflow/ response flow (incepem sa trimitem date catre frontend)
        for (Timber t : listOfTimberByParameters) {
            TimberDTO tDTO = new TimberDTO();
            tDTO.setQuantity(t.getQuantity());
            tDTO.setPrice(t.getPrice());
            tDTO.setWoodType(t.getWoodType());
            tDTO.setHeight(t.getHeight());
            tDTO.setLength(t.getLength());
            listOfTimberDTOByParameters.add(tDTO);
        }
        return listOfTimberDTOByParameters;
    }


    public void addBeam(TimberDTO timberDTO) {
        Timber timber = new Timber();
        timber.setWoodType(timberDTO.getWoodType());
        timber.setHeight(timberDTO.getHeight());
        timber.setLength(timberDTO.getLength());
        timber.setPrice(timberDTO.getLength());
        timber.setQuantity(timberDTO.getQuantity());
        timberDAO.addTimber(timber);
    }

    public void updateTimber(String woodType) {
    }
}
